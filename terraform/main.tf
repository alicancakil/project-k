terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-west-2"
  profile = "k8s"
}

# Get AWS account ID information
data "aws_caller_identity" "current" {}


# Note: It feels like using 'aws_iam_role_policy_attachment' is right way to do this but I  ended up using 'aws_iam_policy_document' data source to generate AMI policy document instead.
# I will do some reading on terraform best practices :)

# Generate an IAM role policy
data "aws_iam_policy_document" "prod-ci-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
        type = "AWS"
        identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}

# Generate an IAM group policy
data "aws_iam_policy_document" "prod-ci-group-policy" {
    statement {
        actions = ["sts:AssumeRole"]
        resources = [aws_iam_role.prod-ci-role.arn]
    }
}

# # Create IAM role with IAM role policy
resource "aws_iam_role" "prod-ci-role" {
    name = "prod-ci-role"
    assume_role_policy = data.aws_iam_policy_document.prod-ci-role-policy.json
}

# Create IAM user
resource "aws_iam_user" "prod-ci-user" {
  name = "prod-ci-user"
}

# Create IAM group
resource "aws_iam_group" "prod-ci-group" {
  name = "prod-ci-group"
}

# Assign IAM user to the group
resource "aws_iam_user_group_membership" "prod-ci-user-assignment" {
  user = aws_iam_user.prod-ci-user.id

  groups = [
    aws_iam_group.prod-ci-group.id
  ]
}

# Assign policy to IAM group
resource "aws_iam_group_policy" "prod-ci-group-policy" {
    name = "prod-ci-group-policy"
    group = "prod-ci-group"
    policy = data.aws_iam_policy_document.prod-ci-group-policy.json
}