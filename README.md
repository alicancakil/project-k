# Project-K

This project builds Litecoin container and pushes into docker hub.
It also contains some terraform practices and kubernetes stateful sets.

- CI/CD Pipeline can be seen here: [https://gitlab.com/alicancakil/project-k/-/pipelines](https://gitlab.com/alicancakil/project-k/-/pipelines)



## Terraform:
Switch to terraform directory and tweak [AWS profile setting](https://gitlab.com/alicancakil/project-k/-/blob/master/terraform/main.tf#L13) based on your local setup.
and then run:
```
terraform init
terraform apply
```
Note: Tested with `Terraform v1.0.11 on darwin_amd64`


## Kubernetes:
Switch to k8s directory and run:
```
kubectl apply -f statefulset.yml
```

Note: Tested with `EKS on AWS`

## Note:
Please note that I intentionally kept this readme minimal.
